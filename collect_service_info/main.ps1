﻿Param(
    [string]$spreadsheet_id
)

$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent

function get_serial_number()
{
    return (Get-WmiObject -Class Win32_BIOS).SerialNumber
}

function post {
    Param(
        [string]$url,
        [string]$data
    )
    $data =
"--AaB03x
Content-Disposition: form-data; name=`"data`"

$data
--AaB03x--"
    $body = [byte[]][char[]]$data;
    $request = [System.Net.WebRequest]::Create("$url")
    $request.Method = 'POST';
    $request.ContentType='multipart/form-data; boundary=AaB03x'
    $request.KeepAlive = $true;
    $streamWriter = New-Object System.IO.StreamWriter $Request.GetRequestStream()
    $streamWriter.Write($body, 0, $body.Length);
    $streamWriter.Flush()
    $streamWriter.Close()
    $response = $Request.GetResponse();
    $ResponseStream = $Response.GetResponseStream()
    $ReadStream = New-Object System.IO.StreamReader $ResponseStream
    $response_data = $ReadStream.ReadToEnd()
    return $response_data
}

# Using this until there's a better serialize solution for ServiceController
function get_field {
    Param(
        [System.ServiceProcess.ServiceController]$service,
        [string]$field
    )
    switch($field){
        "Name" {
            return $service.Name;
        }
        "Status" {
            return $service.Status;
        }
        "DisplayName" {
            return $service.DisplayName;
        }
        "StartType" {
            $retval = $service.StartType;
            if ($retval -eq $null){
                $name = $service.Name;
                $retval = (Get-WmiObject Win32_Service -Filter "name='$name'").StartMode
            }
            return $retval;
        }
    }
    return "'$field' not mapped for 'System.ServiceProcess.ServiceController'"
}

function generate_and_log_data {
    Param(
        [string]$spreadsheet_id,
        [string]$sheet_name
    )
    $fields = @(
        "Name",
        "Status",
        "DisplayName",
        "StartType"
    )
    $json_string = "["
    # Generate header row
    $json_string += "["
    $j = 0
    while ($j -lt $fields.count){
        $field = $fields[$j]
        $json_string += "`"$field`""
        if ($j -lt $fields.count-1){
            $json_string += ","
        }
        $j++
    }
    $json_string += "]"
    # Generate data rows
    $services = Get-Service
    $i = 0
    if ($services.count -gt 0){
        $json_string += ","
    }
    while ($i -lt $services.count){
        $service = $services[$i]
        $json_string += "["
        $j = 0
        while ($j -lt $fields.count){
            $field = $fields[$j]
            $value = get_field -service $service -field $field
            $json_string += "`"$value`""
            if ($j -lt $fields.count-1){
                $json_string += ","
            }
            $j++
        }
        $json_string += "]"
        if ($i -lt $services.count-1){
            $json_string += ","
        }
        $i++
    }
    $json_string += "]"
    $data = "{
        `"values`": $json_string,
        `"sheet_name`": `"$sheet_name`",
        `"spreadsheet_id`": `"$spreadsheet_id`"
    }"
    $retval = post -url "https://script.google.com/a/lbl.gov/macros/s/AKfycbwEI4RRKJbRCS8hgMGgqhhlL-3eSAu7m1Fy7-Nzuri3rHSNbttG/exec" -data $data
    echo $retva > response.html
    return $retval
}

generate_and_log_data -spreadsheet_id $spreadsheet_id -sheet_name $env:COMPUTERNAME
#generate_and_log_data -spreadsheet_id "1QbB90gD8hpgspByx7t5FyRMBdk0WapBMRhLM7SgX7uE" -sheet_name $env:COMPUTERNAME