﻿[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://notepad-plus-plus.org/repository/7.x/7.5/npp.7.5.Installer.x64.exe"
$working_directory = $env:SystemDrive + "\Options"
$target_file = "$working_directory\npp.7.5.Installer.x64.exe"
mkdir $working_directory -Force
(New-Object Net.WebClient).DownloadFile($url, $target_file)
start -FilePath $target_file -ArgumentList "/S" -Wait
