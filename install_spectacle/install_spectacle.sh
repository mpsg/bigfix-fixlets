#!/bin/bash
url="http://setup.lbl.gov/files/support/Spectacle1.2.dmg"
dmg="spectacle.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Install
cp -r "./$dir/Spectacle.app" "/Applications/Spectacle.app"
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
