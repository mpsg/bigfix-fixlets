function get-updates(){
    $updateSession = new-object -com "Microsoft.Update.Session"
    $updates = $updateSession.CreateupdateSearcher().Search("").Updates
    return $updates
}

function enumerate-updates {
    Param(
        [string]$path,
        [System.Object[]]$updates
    )
    Remove-Item -Path $path -Force -ErrorAction SilentlyContinue | Out-Null
    New-Item -Path $path -Force | Out-Null
    if ($updates -eq $null){
        return
    }
    ForEach($update in $updates){
        New-ItemProperty -Path $path -Value $update.Description -Name $update.Title -Force | Out-Null
    }
}

function write-updates-to-registry {
    Param(
        [System.Object[]]$updates
    )
    $enumeration_path = "HKLM:\SOFTWARE\LBNL\ICUSW\Updates"
    $hidden_updates = $updates | where { $_.isHidden -eq $true }
    $pending_updates = $updates | where { $_.isInstalled -eq $false }
    enumerate-updates -Path "$enumeration_path\hidden" -updates $hidden_updates
    enumerate-updates -Path "$enumeration_path\pending" -updates $pending_updates
}

$updates = get-updates
write-updates-to-registry -updates $updates