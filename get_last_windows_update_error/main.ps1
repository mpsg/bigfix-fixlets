#
# "Mon, 26 Apr 2010 12:51:55 -0700" as time
#
$last_error = Get-WinEvent -LogName "Microsoft-Windows-WindowsUpdateClient/Operational" | where { $_.id -eq 25 } | select-object -first 1 | % { $_.TimeCreated.ToString("ddd, d MMM yyyy HH:mm:ss -0700") }
New-Item -Path HKLM:\SOFTWARE\LBNL\ICUSW -Force
New-ItemProperty -Path HKLM:\SOFTWARE\LBNL\ICUSW -Name LastWindowsUpdateError -Value $last_error
[console]::beep(440,500)