#!/usr/bin/env ruby

class Application
  def initialize
    if not (Process::uid == 0)
      puts "Must be run as root"
      exit 1
    end
  end
  def logged_in_user
    users = `users`.split(' ').select {|user| user != "root"}
    retval = users.first
    if (retval.nil?)
      puts "Could not detect logged in user"
      exit 2
    end
    retval
  end
  def main(url)
    user = logged_in_user
    result = IO.popen(['sudo', '-H', '-u', user, 'open', '-a', 'Safari', url])
  end
end

app = Application.new
app.main(*ARGV)
