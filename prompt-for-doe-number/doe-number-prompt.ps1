﻿#Button Types
#
#Value  Description
#0 Show OK button.
#1 Show OK and Cancel buttons.
#2 Show Abort, Retry, and Ignore buttons.
#3 Show Yes, No, and Cancel buttons.
#4 Show Yes and No buttons.
#5 Show Retry and Cancel buttons.

[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')

function tell-user {
    Param(
        [string]$msg
    )
    $shell = new-object -comobject wscript.shell
    $shell.popup($msg, 0, "Berkeley Lab IT") | Out-Null
}

function is-doe-property {
    $shell = new-object -comobject wscript.shell
    $answer = $shell.popup(
        "Does this machine have a DOE asset tag? `n `n To verify the authenticity of this message, contact security@lbl.gov.",
        0,
        "Berkeley Lab IT",
        4
    )
    If ($answer -eq 6) {
        return $true
    }
    return $false
}

function get-doe-number {
    #trying to use messageboxes instead of hardcoding textboxes. https://michlstechblog.info/blog/powershell-show-a-messagebox/
    #combining messagrbox with user input prompt https://community.spiceworks.com/topic/148538-powershell-how-to-prompt-to-enter-a-variable
    $DOEnumber = [Microsoft.VisualBasic.Interaction]::InputBox("Please enter the 7-digit DOE number. `n `n To verify the authenticity of this message, contact security@lbl.gov.", "Berkeley Lab IT")
    $IsDOEProperty = $true
    $retval = @{
        is_doe_property = $IsDOEProperty
        asset_doe_number_manual_entry = $DOENumber
    }
    return $retval
    #write-registrykey -path "HKLM:\SOFTWARE\LBNL\ICUSW\is_doe_property" -value $IsDOEProperty
    #write-registrykey -path "HKLM:\SOFTWARE\LBNL\ICUSW\asset_doe_number_manual_entry" -value $DOENumber
}

function write-registrykey {
    Param(
        [string]$path,
        [string]$value
    )
    $chunks = [System.Collections.ArrayList]$path.split("\") # Cast to type that allows removing elements
    $key = $chunks[$chunks.Count - 1]
    $chunks.RemoveAt($chunks.Count - 1)
    $path = $chunks -join "\"
    GetOrCreate-Item -Path $path | Out-Null
    New-ItemProperty -Path $path -Value $value -Name $key -Force | Out-Null
}

function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function main {
    $is_doe = $false
    $doe_number = ""
    if (is-doe-property){
        $doe_number = get-doe-number
        $is_doe = $true
    }
    tell-user "Thank you"
    $data = @{
        is_doe_property = $is_doe
        asset_doe_number_manual_entry = $doe_number
    } | ConvertTo-Json
    New-Item -Path ~\tmp -ItemType Directory -ErrorAction SilentlyContinue 
    Set-Content -Path ~\tmp\doe_prompt_response.json -Value $data -Force
}

main