﻿function log {
    Param(
        [string]$msg,
        [string]$log_prefix = "C:\Options\Logs",
        [string]$file_name = "$(split-path $MyInvocation.PSCommandPath -Leaf).log"
    )
    New-Item -ItemType Directory -Path $log_prefix -Force -ErrorAction SilentlyContinue | Out-Null
    $log_file_path = "$log_prefix\$file_name"
    $date = date
    $line = "$date`: $msg"
    $line | Out-File -FilePath $log_file_path -Append
    write-host $line | out-null
}

function main {
    New-Item -ItemType Directory -Path "C:\Options\PowershellViaActionscript" -Force -ErrorAction Stop
    New-Item -ItemType File -Path "C:\Option\PowershellViaActionscript" -ErrorAction Stop
}

try {
    main
} catch {
    $string_err = $_ | Out-String
    log -msg $string_err
}