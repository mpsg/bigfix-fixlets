﻿[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function download_file {
    Param (
        [string]$Url,
        [string]$Path
    )
    $client = New-Object Net.WebClient
    $download_attempt=0
    while ($download_attempt -lt 3){
        $client.DownloadFile($Url, $Path)
        sleep 1
        $exists = Test-Path -Path $Path
        if ($exists){
            break
        }
        $download_attempt++
    }
    return $exists
}
$url = "http://ardownload.adobe.com/pub/adobe/reader/win/AcrobatDC/1701220093/AcroRdrDC1701220093_en_US.exe"
$config_url = "http://setup.lbl.gov/files/support/AcroRead.txt"
$working_directory = $env:SystemDrive + "\Options"
$installer_directory = "$working_directory\AcroRdrDC1701220093_en_US"
$target_file = "$working_directory\AcroRdrDC1701220093_en_US.exe"
$msiexec = $env:SystemDrive + "\Windows\System32\msiexec.exe"
$config_file = "$installer_directory\AcroRead.mst"
$installer_file = "$installer_directory\AcroRead.msi"
rm -rf $installer_directory
rm -rf $installer_file
mkdir $working_directory -Force
mkdir $installer_directory -Force
download_file -Url $url -Path $target_file
start -FilePath $target_file -ArgumentList "-sfx_o`"$installer_directory`" -sfx_ne" -Wait
download_file -Url $config_url -Path $config_file
start -FilePath $msiexec -ArgumentList "/i $installer_file /q TRANSFORMS=$config_file" -Wait
