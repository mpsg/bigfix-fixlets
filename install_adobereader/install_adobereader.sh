#!/bin/bash
url="http://setup.lbl.gov/files/support/AcroRdrDC_1701220093_MUI.dmg"
dmg="adobe.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
# Not sure how to remove
# Install new
installer -pkg ./$dir/*.pkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
