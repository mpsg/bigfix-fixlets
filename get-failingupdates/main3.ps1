﻿function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function psobject_to_hash($object){
    $retval = @{}
    $object.psobject.properties | Foreach { $retval[$_.Name] = $_.Value }
    return $retval
}

function ConvertcomtoHash($com){
    $retval = @{}
    $com.psobject.properties | Foreach {
        $key = $_.Name
        $value = $_.Value
        if ($value -eq $null){
            $value = ""
        }
        if ($value.GetType().ToString() -ne "System.__ComObject"){
            $retval[$key] = $value
            if ($key -eq "Date"){
                $retval[$key] = [DateTime]$value
            }
        }
    }
    return $retval
}

function Get-UpdateLog {
    $Session = New-Object -ComObject "Microsoft.Update.Session" 
    $Searcher = $Session.CreateUpdateSearcher() 
    $historyCount = $Searcher.GetTotalHistoryCount() 
    $update_logs = $Searcher.QueryHistory(0, $historyCount)
    $retval = @()
    ForEach($log in $update_logs){
        $hashed_log = ConvertcomtoHash($log)
        $retval += $hashed_log
    }
    return $retval
}

function Get-UpdateStatuses {
    $retval = @{}
    $update_logs = Get-UpdateLog
    ForEach($log in $update_logs){
        $set = $false
        if ($retval[$log['Title']] -eq $null){
            $set = $true
        } else {
            if ($log['Date'] -gt $retval[$log['Date']]){
                $set = $true
            }
        }
        if ($set){
            $retval[$log['Title']] = $log
        }
    }
    return $retval
}

function Write-Registry {
    Param(
        [String]$path,
        [Hashtable]$hash
    )
    New-Item -Path $path -Force | Out-Null
    ForEach ($title in $hash.Keys){
        $update_status = $hash[$title]
        New-Item -Path "$path/$title" | Out-Null
        ForEach ($key in $update_status.Keys){
            New-ItemProperty -Path "$path/$title" -Name $key -Value $update_status[$key] | Out-Null
        }
    }
}

function main {
    $update_statuses = Get-UpdateStatuses
    Write-Registry -path "HKLM:\SOFTWARE\LBNL\ICUSW\Windows\UpdateStatuses" -objects $update_statuses
}

main