require 'net/http'
require 'json'
require 'fileutils'
class IcuswComputer
  def initialize
    @working_directory = "/opt/lbl/icusw"
    @serial_number = `ioreg -l | grep IOPlatformSerialNumber | cut -d '=' -f 2 | sed 's/"//g'`.strip
    @good_working_directory = false
    begin
      FileUtils.mkdir_p @working_directory
      @good_working_directory = true
    rescue
      puts "Warning: Couldn't create working directory"
    end
  end

  def property_data_from(server)
    url = URI("#{server}/api/property_data_for?serial_number=" + @serial_number)
    response = Net::HTTP.get(url) # => String
    JSON.parse(response)
  end

  def write_property_data_from(server)
    if !@good_working_directory then
      puts "Warning: Not writing keys, because couldn't verify that working directory exists"
      return
    end
    write_data_helper = lambda do |directory, object|
      object.each do |key, value|
        new_path = "#{directory}/#{key}"
        if (value.class == Hash)
          FileUtils.mkdir_p new_path
          write_data_helper.call new_path, value
        else
          File.open(new_path, 'w') do |file|
            file.write(value)
          end
        end
      end
    end
    data = property_data_from(server)
    write_data_helper.call @working_directory, data
  end
end

if ARGV.length > 0
  method = ARGV[0].to_sym
  ARGV.delete_at 0
  localhost = IcuswComputer.new
  localhost.send(method, *ARGV)
end
