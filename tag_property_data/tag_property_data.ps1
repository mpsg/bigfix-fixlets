﻿Param(
    [string]$datasource_url
)

# This is a shim for where JSON parsing is not available. Previously used system.web.script for JSON deserialization but later found that it's only available in .NET 3.5
$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json_shim.ps1"
Import-Module -Name $json_file -Force

function ConvertFrom-JsonShim([object] $item){
    #add-type -assembly system.web.extensions
    #$ps_js=new-object system.web.script.serialization.javascriptSerializer

    #The comma operator is the array construction operator in PowerShell
    #return $ps_js.DeserializeObject($item)
    return [Procurios.Public.JSON]::JsonDecode($item)
}

function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function Download-String {
    Param(
        [string]$url
    )
    if ([Net.ServicePointManager]::SecurityProtocol.ToString().Contains("Tls12")){
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $WebClient = New-Object System.Net.WebClient
        $retval = $WebClient.DownloadString($url)
    } else {
        $curl = "&`"$PSScriptRoot\curl-7.66.0-win32-mingw.exe`""
        $retval = iex "$curl --cacert `"$PSScriptRoot\icusw-ca-bundle.crt`" -L $url"
    }
    return $retval
}

function psobject_to_hash($object){
    $retval = @{}
    $object.psobject.properties | Foreach { $retval[$_.Name] = $_.Value }
    return $retval
}

function dump-json-at
{
    Param(
        [string]$Path,
        [PSCustomObject]$Object,
        [int]$count
    )
    GetOrCreate-Item -Path $path | Out-Null
    if ($Object -eq $null){
        return 0
    }
    if ($Object.getType().ToString() -like "*PSCustomObject*"){
        $Object = psobject_to_hash($Object)
    }
    foreach ($key in $Object.keys){
        $value = $Object[$key]
        if ($value -ne $null){
            if (
                $value.gettype().toString() -like "*Dictionary*" -or
                $value.gettype().name -like "*Hashtable*" -or
                $value.gettype().toString() -like "*PSCustomObject*"
            ){
                $count += (dump-json-at -Path "$path\$key" -Object $value)
            } else {
                New-ItemProperty -Path $path -Value $value -Name $key -Force | Out-Null
                $count += 1
            }
        }
    }
    return $count
}

function write_property_data_from($from)
{
    $data = property_data_from($from)
    if ($data -ne $null){
        log -msg "Writing data into the registry"
        GetOrCreate-Item -Path "HKLM:\SOFTWARE\LBNL"
        $path = "HKLM:\SOFTWARE\LBNL\ICUSW"
        $records_written = dump-json-at -Path $path -Object $data -count 0
        log -msg "Wrote $records_written records"
    } else {
        log -msg "write_property_data_from tried to process null `$data"
    }
}

function log {
    Param(
        [string]$msg
    )
    $log_folder = "$env:SystemDrive\Options\tag_property_data"
    $result = mkdir $log_folder -Force
    $timestamp = Get-Date -UFormat "%Y%m%d %H:%M:%S"
    $msg = $timestamp + ": $msg"
    $msg | Add-Content -Path "$log_folder\log.txt"
    Write-Host $msg
}

function property_data_from($server)
{
    $url = $server + "/api/property_data_for?serial_number=$serial_number"
    try {
        $raw_json = Download-String -url $url
        log -msg "Retrieved property data"
    } catch {
         #while ($true){ $input = Read-Host -Prompt "repl"; if ($input -eq "exit"){ return 0 }; Invoke-Expression $input }
         log -msg $Error
         return $null
    }
    return ConvertFrom-JsonShim $raw_json
}

function get_serial_number()
{
  $retval = (Get-WmiObject -Class Win32_BIOS).SerialNumber
  log -msg "Retrieved serial number: $retval"
  return $retval
}

$serial_number = get_serial_number
if ($datasource_url -eq "") {
    log -msg "Script called without datasource_url"
} else {
    write_property_data_from($datasource_url)
}
