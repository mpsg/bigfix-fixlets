#!/bin/bash

username=$1
public_key=$2
path="~$username"
ssh_dir="./.ssh"
authorized_keys_file="$ssh_dir/authorized_keys"

eval cd $path
mkdir -p $ssh_dir
touch $authorized_keys_file
chmod 700 $ssh_dir
chmod 600 $authorized_keys_file
echo $public_key >> $authorized_keys_file
