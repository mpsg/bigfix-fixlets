﻿[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$url = "https://download.mozilla.org/?product=firefox-55.0.3-SSL&os=win64&lang=en-US"
$working_directory = $env:SystemDrive + "\Options"
$target_file = "$working_directory\FirefoxSetup.exe"
mkdir $working_directory -Force
(New-Object Net.WebClient).DownloadFile($url, $target_file)
start -FilePath $target_file -ArgumentList "-ms" -Wait
