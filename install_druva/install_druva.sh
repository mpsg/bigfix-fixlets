#!/bin/bash
url="http://setup.lbl.gov/files/support/inSync-5.9.5-r58373.dmg"
dmg="druva.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
# Not sure how to remove
# Install new
installer -pkg ./$dir/*.mpkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
