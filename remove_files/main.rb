#!/usr/bin/ruby

require 'date'
require 'json'

def log(text)
  logfile = "/var/log/remove_files.log"
  log = "#{DateTime.now} #{text}\n"
  File.open(logfile, 'a') { |file| file.write(log) } if !log.empty?
end

log("JSON Input")
log(ARGV.to_json)

ARGV.each do |fs_objs|
  fs_objs.split("\n").each do |fs_obj|
    cmd = "rm -rf '#{fs_obj.strip.gsub("\r", "")}'"
    `#{cmd}`
    err_code = `echo $?`.strip
    debug_info = "#{err_code}: #{cmd}"
    log(debug_info)
  end
end
