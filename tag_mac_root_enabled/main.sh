#!/bin/bash
path=/opt/lbl/icusw
mkdir -p $path
char_count=`dscl . -read /Users/root | grep "Password: \*\*" | wc -c`
if [ $char_count -eq 0 ]; then
  echo "false" > /opt/lbl/icusw/root_enabled
else
  echo "true" > /opt/lbl/icusw/root_enabled
fi
