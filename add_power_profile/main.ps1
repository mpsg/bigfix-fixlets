﻿[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$LBLPower = "9ca7a9ca-1ca3-40aa-aa6d-205edf00030a"
$powerplan_file = "$env:SystemDrive\Options\PowerPlan.pwr"

function download-profile {
    $WebClient = New-Object System.Net.WebClient
    $WebClient.DownloadFile("https://bitbucket.org/tasaif/bigfix-fixlets/raw/master/add_power_profile/PowerPlan.pwr", $powerplan_file)
}

function ensure-optionsfolder {
    mkdir $env:SystemDrive\Options -Force
}

function apply-powerconfig {
    powercfg -h off
    powercfg -x -hibernate-timeout-ac 0
    powercfg -x -hibernate-timeout-dc 0
    powercfg -x -monitor-timeout-ac 15
    powercfg -x -monitor-timeout-dc 15
    powercfg -x -disk-timeout-ac 0
    powercfg -x -disk-timeout-dc 0
    powercfg -x -standby-timeout-ac 0
    powercfg -x -standby-timeout-dc 0
    powercfg -import $powerplan_file $LBLPower
    powercfg -setactive $LBLPower
    powercfg -changename $LBLPower "LBNL Power Plan" "Pre-configured plan favoring maximum performance."
}

function remove-powerconfig {
    $balancedpower = "381b4222-f694-41f0-9685-ff5bb260df2e"
    $result = powercfg /GETACTIVESCHEME
    $activescheme = $result.split()[3]
    if ($activescheme -eq $LBLPower){
        powercfg -setactive $balancedpower
    }
    powercfg /D $LBLPower
}

function main {
    ensure-optionsfolder
    download-profile
    apply-powerconfig
}

main
