@echo off
REM 4:16 PM 10/20/2010
set LBLPower=9ca7a9ca-1ca3-40aa-aa6d-205edf00030a
powercfg -h off >nul
powercfg -x -hibernate-timeout-ac 0 >nul
powercfg -x -hibernate-timeout-dc 0 >nul
powercfg -x -monitor-timeout-ac 15 >nul
powercfg -x -monitor-timeout-dc 15 >nul
powercfg -x -disk-timeout-ac 0 >nul
powercfg -x -disk-timeout-dc 0 >nul
powercfg -x -standby-timeout-ac 0 >nul
powercfg -x -standby-timeout-dc 0 >nul
echo Importing power plan...
powercfg -import "%~dp0/PowerPlan.pwr" %LBLPower%
powercfg -setactive %LBLPower%
powercfg -changename %LBLPower% "LBNL Power Plan" "Pre-configured plan favoring maximum performance."