$baselines = $baselines.split();
$base_path = "HKLM:\SOFTWARE\LBNL\baselines";
New-Item -Path $base_path -Force
ForEach($baseline in $baselines) {
    New-ItemProperty -Path $base_path -Name $baseline
}