#!/bin/bash

#grabbing the serial number so we can use it to search in api
serial=$(system_profiler SPHardwareDataType | awk '/Serial/ {print $4}')
hostName=$(hostname)

#api call to get custodian info
api_call=$(curl https://icusw.lbl.gov/api/property_data_for?serial_number="${serial}")
LDAP=$(echo "$api_call" | grep -o '"epo":"[^"]*' | sed 's/"epo":"\([^@]*\)@.*/\1/')
DOE=$(echo "$api_call" | grep -o '"asset_tag_nr":"[^"]*' | sed 's/"asset_tag_nr":"//')
newName="${LDAP}"-M"${DOE: -2}"

if [[ "${newName}" != '-M' ]]; then
    #set the names to the correct or at least more identifable name 
    sudo scutil --set ComputerName "${newName}"
    sudo scutil --set LocalHostName "${newName}"
    sudo scutil --set HostName "${newName}"

    #flush the DNS cache
    dscacheutil -flushcache
fi 
