# Uninstalls products using msiexec or innosetup and has product specific removal steps
function Uninstall-KnownProduct {
    Param(
        [psobject]$uninstall_information
    )
    Write-Host "Removing $($uninstall_information.DisplayName)"
    $uninstall_string = $uninstall_information.UninstallString
    $run_uninstall = $false
    if ($uninstall_string.ToLower().Trim("`"").StartsWith("msiexec")){
        $cmd = "$uninstall_string /norestart /quiet"
        $run_uninstall = $true
    }
    if ($uninstall_string.ToLower().Trim("`"").EndsWith("unins000.exe")){
        $cmd = "$uninstall_string /norestart /verysilent"
        $run_uninstall = $true
    }
    if ($run_uninstall){
        & cmd /c $cmd
        if ($? -eq $false){
            Write-Host "Command Failed: $cmd"
        }
    } else {
        Write-Host "Not sure how to uninstall the following"
        ForEach($key in $uninstall_information.Keys){
            Write-Host "$($key): $($uninstall_information[$key])"
        }
    }
    if ($uninstall_information.DisplayName.ToLower().Contains("pgina")){
        Remove-Item 'HKLM:\SOFTWARE\pGina3' -Recurse -Force;
        Remove-Item 'C:\Program Files\pGina' -Recurse -Force;
    }
    if ($uninstall_information.DisplayName.ToLower().Contains("linotp")){
        if ((Get-WmiObject Win32_OperatingSystem).OSArchitecture -eq "32-bit"){
            Remove-Item 'C:\Program Files\LinOTP Authentication Provider' -Recurse -Force;
        } else {
            Remove-Item 'C:\Program Files\LinOTP Authentication Provider (64 bit)' -Recurse -Force;
        }
    }
}

# Identifies products in Programs and Features and runs a custom uninstall function against them "Uninstall-KnownProduct"
function Uninstall-Product {
    Param(
        [String]$Name
    )
    $productNames = @($Name)
    $UninstallKeys = @('HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall')
    if ((Get-WmiObject Win32_OperatingSystem).OSArchitecture -ne "32-bit"){
        $UninstallKeys += @('HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall')
    }
    foreach ($key in (Get-ChildItem $UninstallKeys) ) {
        foreach ($product in $productNames) {
            if ($key.GetValue("DisplayName") -like "$product") {
                $uninstall_information = @{
                    KeyName = $key.Name.split('\')[-1]
                    DisplayName = $key.GetValue("DisplayName")
                    UninstallString = $key.GetValue("UninstallString")
                    Publisher = $key.GetValue("Publisher")
                }
                Uninstall-KnownProduct -uninstall_information $uninstall_information
            }
        }
    }
}

function Uninstall-OfflineProvider {
    $service = Get-Service | where { $_.Name -contains "LBLStrongIDOfflineProvider" }
    if ($service -eq $null){
        return
    }
    Write-Host "Uninstalling Offline Provider"
    if ($service.Status -ne "Stopped"){
        $service.Stop()
    }
    $LBL_SYSTEM_FOLDER = "$ENV:SYSTEMROOT\System32\LBL-LAP"
    $nssm = "$LBL_SYSTEM_FOLDER\NSSM.exe";
    Start-Process -Wait -NoNewWindow $nssm -ArgumentList "remove LBLStrongIDOfflineProvider confirm"
    Remove-Item -ErrorAction Continue -Recurse -Force $LBL_SYSTEM_FOLDER
}

function Remove-Provider {
    Param(
        [string]$Name
    )
    Write-Host "Removing Provider: $Name"
    $credential_providers = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\Credential Providers";
    $credential_provider_filters = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\Credential Provider Filters";
    $provider = Get-ChildItem $credential_providers | where { ($_ | Get-ItemProperty)."(default)" -eq $name }
    if ($provider -eq $null){
        Write-Host "Credential Provider '$Name' not found"
        return
    }
    $guid = Split-Path $provider.PSPath -Leaf
    Remove-Item "$credential_providers\$guid"
    Remove-Item "$credential_provider_filters\$guid"
}

function Set-LastProvider {
    Param(
        [string]$Name
    )
    Write-Host "Setting Last Provider: $Name"
    $credential_providers = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\Credential Providers";
    $provider = Get-ChildItem $credential_providers | where { ($_ | Get-ItemProperty)."(default)" -eq $name }
    if ($provider -eq $null){
        Write-Host "Credential Provider '$Name' not found"
        return
    }
    $guid = Split-Path $provider.PSPath -Leaf
    Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI" -Name "LastLoggedOnProvider" -Value $guid
}

function Configure-CredentialProviders {
    Write-Host 'Configuring last-used logon providers'
    $providers_to_remove = @(
        "LinOTPCredentialProvider",
        "pGinaCredentialProvider"
    )
    ForEach ($provider in $providers_to_remove) {
        Remove-Provider $provider
    }
    Set-LastProvider -Name "PasswordProvider"
}

Uninstall-Product -Name "*LinOTP Authentication Provider*"
Uninstall-Product -Name "*StrongID*"
Uninstall-Product -Name "*pgina*"
Uninstall-OfflineProvider
Configure-CredentialProviders