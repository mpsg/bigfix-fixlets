#!/bin/bash
url="http://setup.lbl.gov/files/support/googlechrome.dmg"
dmg="chrome.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Install
cp -r "./$dir/Google Chrome.app" "/Applications/Google Chrome.app"
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
