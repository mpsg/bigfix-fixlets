﻿Param(
    [string]$token
)

# This is a shim for where JSON parsing is not available. Previously used system.web.script for JSON deserialization but later found that it's only available in .NET 3.5
$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json_shim.ps1"
Import-Module -Name $json_file -Force

function ConvertFrom-JsonShim([object] $item){
    #add-type -assembly system.web.extensions
    #$ps_js=new-object system.web.script.serialization.javascriptSerializer

    #The comma operator is the array construction operator in PowerShell
    #return $ps_js.DeserializeObject($item)
    return [Procurios.Public.JSON]::JsonDecode($item)
}

function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function Download-String {
    Param(
        [string]$url
    )
    if ($false -and [Net.ServicePointManager]::SecurityProtocol.ToString().Contains("Tls12")){
        log -msg "Using System.Net.WebClient"
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $WebClient = New-Object System.Net.WebClient
        $retval = $WebClient.DownloadString($url)
    } else {
        log -msg "Using curl"
        $curl = "&`"$PSScriptRoot\curl-7.66.0-win32-mingw.exe`""
        $retval = iex "$curl --cacert `"$PSScriptRoot\cacert.pem`" -L '$url'"
    }
    return $retval
}

function WriteJsonTo-Registry {
    Param(
        [string]$Path,
        [string]$json_string
    )
    $helper = {
        Param(
            [String]$store,
            [String[]]$path,
            [Object]$obj
        )
        # If array, create a regkey under the given path with the index as the name then recurse for the values
        # If string, create an attribute named value at the given node
        # If hash, create a regkey with that name then recurse for the values
        $string_curpath = "$($store):$($path -join "\")"
        $curpath = Get-ChildItem -Path $string_curpath
        if ($obj -is [Hashtable]){
            ForEach($key in $obj.Keys){
                $arr_new_path = $path + @($key)
                $string_new_path = "$(($arr_new_path) -join "\")"
                New-Item -Path "$($store):$($string_new_path)" | Out-Null
                $helper.Invoke($store, $string_new_path, $obj[$key])
            }
        }
        if (($obj -is [String]) -or ($obj -is [ValueType])){
            Set-ItemProperty -Path $string_curpath -Name "value" -Value $obj | Out-Null
        }
        if (($obj -is [System.Collections.ArrayList]) -or ($obj -is [System.Object[]])) {
            $i = 0
            while ($i -lt $obj.Count){
                $arr_new_path = $path + @($i.ToString())
                $string_new_path = "$(($arr_new_path) -join "\")"
                New-Item -Path "$($store):$($string_new_path)" | Out-Null
                $helper.Invoke($store, $string_new_path, $obj[$i])
                $i += 1
            }
        }
    }
    $store = ($path -split ":")[0]
    $_path = $(($path -split ":")[1] -split '\\')
    $obj = ConvertFrom-JsonShim -item $json_string
    $helper.Invoke($store, $_path, $obj)
}

function log {
    Param(
        [string]$msg
    )
    $log_folder = "$env:SystemDrive\Options\logs"
    $result = mkdir $log_folder -Force
    $timestamp = Get-Date -UFormat "%Y%m%d %H:%M:%S"
    $msg = $timestamp + ": $msg"
    $msg | Add-Content -Path "$log_folder\tag_windows_7_upgrade_exception_data.log"
    Write-Host $msg
}

function Tag-ExceptionData {
    Param(
        [string]$hostname
    )
    $path = "HKLM:\SOFTWARE\LBNL\ICUSW\windows_7_upgrade_exception_data"
    $json = Download-String -url "https://script.google.com/macros/s/AKfycbwaRBHRwto9gu17vxvzyI9vdkypWWJdq9cWQXyPLA0sAQpqvwgR/exec?token=$token&hostname=$hostname"
    log -msg $json
    New-Item -Path $path -Force | Out-Null
    log -msg "Writing data into the registry"
    WriteJsonTo-Registry -Path $path -json_string $json
}

Tag-ExceptionData -hostname $env:COMPUTERNAME