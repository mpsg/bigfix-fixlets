function create-login-page-task
{
    param(
        [string]$url,
        [string]$name
    )
    $A = New-ScheduledTaskAction �Execute "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" -Argument "$url"
    $T = New-ScheduledTaskTrigger -AtLogOn -User "Training"
    $S = New-ScheduledTaskSettingsSet
    $P = New-ScheduledTaskPrincipal -UserId "Training"
    $D = New-ScheduledTask -Action $A -Trigger $T -Settings $S -Principal $P
    Register-ScheduledTask $name -InputObject $D
}

create-login-page-task -url "https://commons.lbl.gov/display/itdivision/Training+-+User" -name StudentLoginTask
