#!/bin/bash
url="http://setup.lbl.gov/files/support/anyconnect-macosx-i386-4.3.05017-k9-LBL.dmg"
dmg="anyconnect.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
/opt/cisco/anyconnect/bin/vpn_uninstall.sh
# Install new
installer -pkg ./$dir/*.pkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
