﻿function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function psobject_to_hash($object){
    $retval = @{}
    $object.psobject.properties | Foreach { $retval[$_.Name] = $_.Value }
    return $retval
}

function ConvertcomtoHash($com){
    $retval = @{}
    $com.psobject.properties | Foreach {
        $key = $_.Name
        $value = $_.Value
        if ($value -eq $null){
            $value = ""
        }
        if ($value.GetType().ToString() -ne "System.__ComObject"){
            $retval[$key] = $value
            if ($key -eq "Date"){
                $retval[$key] = [DateTime]$value
            }
        }
    }
    return $retval
}

function Get-UpdateLog {
    $tag_update_operation_enum_to_string = @(
        "uoUnknown",
        "uoInstallation",
        "uoUninstallation",
        "uoOther"
    )
    $tag_operation_result_code_enum_to_string = @(
        "orcNotStarted",
        "orcInProgress",
        "orcSucceeded",
        "orcSucceededWithErrors",
        "orcFailed",
        "orcAborted"
    )
    $Session = New-Object -ComObject "Microsoft.Update.Session" 
    $Searcher = $Session.CreateUpdateSearcher()
    $historyCount = $Searcher.GetTotalHistoryCount() 
    $update_logs = $Searcher.QueryHistory(0, $historyCount)
    $retval = @()
    ForEach($log in $update_logs){
        $hashed_log = ConvertcomtoHash($log)
        $hashed_log["tagUpdateOperation"] = $tag_update_operation_enum_to_string[$hashed_log["Operation"]]
        $hashed_log["tagOperationResultCode"] = $tag_operation_result_code_enum_to_string[$hashed_log["ResultCode"]]
        $hashed_log["UpdateID"] = $log.UpdateIdentity.UpdateID
        $retval += $hashed_log
    }
    return $retval
}

function Get-UpdateStatuses {
    # This function returns the latest status of each update available in the Windows Update log
    # If an installation was attempted on the same update multiple times, only the latest status is returned
    $retval = @{}
    $update_logs = Get-UpdateLog
    ForEach($log in $update_logs){
        $set = $false
        if ($retval[$log['Title']] -eq $null){
            $set = $true
        } else {
            if ($log['Date'] -gt $retval[$log['Title']]['Date']){
                $set = $true
            }
        }
        if ($set){
            $retval[$log['Title']] = $log
        }
    }
    return $retval
}

function Get-PendingUpdates {
    $Session = New-Object -ComObject "Microsoft.Update.Session" 
    $Searcher = $Session.CreateUpdateSearcher()
    $pending = $Searcher.Search("")
    $retval = @()
    ForEach($update in $pending.Updates){
        $hash = ConvertcomtoHash($update)
        $hash['UpdateId'] = $update.Identity.UpdateId
        $retval += $hash
    }
    return $retval
}

function Get-FailedStatusUpdates {
    $update_statuses = Get-UpdateStatuses
    $retval = @()
    ForEach($key in $update_statuses.Keys){
        $update_log = $update_statuses[$key]
        if (($update_log.tagUpdateOperation -eq "uoInstallation") -and ($update_log.tagOperationResultCode -ne "orcSucceeded")){
            $retval += $update_log
        }
    }
    return $retval
}

function Get-FailingUpdates {
    # Here we define a failing update as an update that is pending and the latest status in the update log is failed
    $failed_statuses = Get-FailedStatusUpdates
    $pending_updates = Get-PendingUpdates
    $retval = @{}
    ForEach($pending_update in $pending_updates){
        $failed_status = $failed_statuses | where { $_.UpdateId -eq $pending_update.UpdateId }
        if ($failed_status -ne $null){
            $retval[$pending_update.Title] = $pending_update
        }
    }
    return $retval
}

function Write-Registry {
    Param(
        [String]$path,
        [Hashtable]$hash,
        [Array]$array
    )
    New-Item -Path $path -Force | Out-Null
    if ($hash -ne $null){
        ForEach ($title in $hash.Keys){
            $update_status = $hash[$title]
            New-Item -Path "$path/$title" | Out-Null
            ForEach ($key in $update_status.Keys){
                New-ItemProperty -Path "$path/$title" -Name $key -Value $update_status[$key] | Out-Null
            }
        }
    }
}

function main {
    $update_statuses = Get-UpdateStatuses
    $failing_updates = Get-FailingUpdates
    Write-Registry -path "HKLM:\SOFTWARE\LBNL\ICUSW\Windows\UpdateStatuses" -hash $update_statuses
    Write-Registry -path "HKLM:\SOFTWARE\LBNL\ICUSW\Windows\FailingUpdates" -hash $failing_updates
    New-ItemProperty -Path "HKLM:\SOFTWARE\LBNL\ICUSW\Windows\UpdateStatuses" -Name "Description" -Value "These are the last known statuses from the Windows Update log and were populated by the tag-updatelogs script" | Out-Null
    New-ItemProperty -Path "HKLM:\SOFTWARE\LBNL\ICUSW\Windows\FailingUpdates" -Name "Description" -Value "These are pending updates whose last known status from the Windows Update log was not 'Success'" | Out-Null
}

main