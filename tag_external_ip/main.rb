ip_result = `nslookup myip.opendns.com. resolver1.opendns.com`
return if $? != 0 # quit if nslookup failed
ips = []
while (match = ip_result.match(/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/)) do
  ips.push match[0]
  ip_result.gsub!(match[0], '')
end
return if ips.length == 0 # quit if no ips were able to be harvested
fh = File.open("/opt/lbl/icusw/external_ip", "w")
fh.write(ips.last)
fh.close()
