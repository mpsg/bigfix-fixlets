﻿function lookup-externalip {
    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.FileName = "nslookup.exe"
    $pinfo.RedirectStandardError = $true
    $pinfo.RedirectStandardOutput = $true
    $pinfo.UseShellExecute = $false
    $pinfo.Arguments = @("myip.opendns.com.", "resolver1.opendns.com")
    $p = New-Object System.Diagnostics.Process
    $p.StartInfo = $pinfo
    $p.Start() | Out-Null
    $p.WaitForExit()
    $stdout = $p.StandardOutput.ReadToEnd()
    $stderr = $p.StandardError.ReadToEnd()
    if ($p.ExitCode -ne 0) {
        return $null
    }
    $ip_result = $stdout.Split() | where { $_ -ne "" }
    $ip_address = $ip_result[-1] # Get last element of nslookup result after filtering out empty lines
    $match = ($ip_address -match "\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")
    if ($match -eq $False) {
        return $null
    }
    return $ip_address
}

function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function write-registrykey {
    Param(
        [string]$path,
        [string]$value
    )
    $chunks = [System.Collections.ArrayList]$path.split("\") # Cast to type that allows removing elements
    $key = $chunks[$chunks.Count - 1]
    $chunks.RemoveAt($chunks.Count - 1)
    $path = $chunks -join "\"
    GetOrCreate-Item -Path $path | Out-Null
    New-ItemProperty -Path $path -Value $value -Name $key -Force | Out-Null
}

$ip = lookup-externalip
if ($ip -eq $null){
    return
}
write-registrykey -path "HKLM:\SOFTWARE\LBNL\ICUSW\external_ip" -value $ip
