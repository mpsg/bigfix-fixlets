﻿Param(
    [string]$spreadsheet_id
)

$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent

function get_serial_number()
{
    return (Get-WmiObject -Class Win32_BIOS).SerialNumber
}

function post {
    Param(
        [string]$url,
        [string]$data
    )
    $data =
"--AaB03x
Content-Disposition: form-data; name=`"data`"

$data
--AaB03x--"
    $body = [system.Text.Encoding]::UTF8.GetBytes($data);
    $request = [System.Net.WebRequest]::Create("$url")
    $request.Method = 'POST';
    $request.ContentType='multipart/form-data; boundary=AaB03x'
    $request.KeepAlive = $true;
    $streamWriter = New-Object System.IO.StreamWriter $Request.GetRequestStream()
    $streamWriter.Write($body, 0, $body.Length);
    $streamWriter.Flush()
    $streamWriter.Close()
    $response = $Request.GetResponse();
    $ResponseStream = $Response.GetResponseStream()
    $ReadStream = New-Object System.IO.StreamReader $ResponseStream
    $response_data = $ReadStream.ReadToEnd()
    return $response_data
}

# Using this until there's a better serialize solution for ServiceController
function get_field {
    Param(
        [System.MarshalByRefObject]$rule,
        [string]$field
    )
    switch($field){
        "Name" { return $rule.Name;}
        "Description" { return $rule.Description;}
        "ApplicationName" { return $rule.ApplicationName;}
        "serviceName" { return $rule.serviceName;}
        "Protocol" { return $rule.Protocol;}
        "LocalPorts" { return $rule.LocalPorts;}
        "RemotePorts" { return $rule.RemotePorts;}
        "LocalAddresses" { return $rule.LocalAddresses;}
        "RemoteAddresses" { return $rule.RemoteAddresses;}
        "IcmpTypesAndCodes" { return $rule.IcmpTypesAndCodes;}
        "Direction" { return $rule.Direction;}
        "Interfaces" { return $rule.Interfaces;}
        "InterfaceTypes" { return $rule.InterfaceTypes;}
        "Enabled" { return $rule.Enabled;}
        "Grouping" { return $rule.Grouping;}
        "Profiles" { return $rule.Profiles;}
        "EdgeTraversal" { return $rule.EdgeTraversal;}
        "Action" { return $rule.Action;}
        "EdgeTraversalOptions" { return $rule.EdgeTraversalOptions;}
        "LocalAppPackageId" { return $rule.LocalAppPackageId;}
        "LocalUserOwner" { return $rule.LocalUserOwner;}
        "LocalUserAuthorizedList" { return $rule.LocalUserAuthorizedList;}
        "RemoteUserAuthorizedList" { return $rule.RemoteUserAuthorizedList;}
        "RemoteMachineAuthorizedList" { return $rule.RemoteMachineAuthorizedList;}
        "SecureFlags" { return $rule.SecureFlags;}
    }
    return "'$field' not mapped for firewall rule object"
}

function get-firewallrules {
    (New-object –comObject HNetCfg.FwPolicy2).rules
}

function escape-string {
    Param(
        [string]$value
    )
    $retval = $value
    $retval = $retval.Replace("\", "\\")
    return $retval
}

function generate_and_log_data {
    Param(
        [string]$spreadsheet_id,
        [string]$sheet_name
    )
    $fields = @(
        "Name",
        "Description",
        "ApplicationName",
        "serviceName",
        "Protocol",
        "LocalPorts",
        "RemotePorts",
        "LocalAddresses",
        "RemoteAddresses",
        "IcmpTypesAndCodes",
        "Direction",
        "Interfaces",
        "InterfaceTypes",
        "Enabled",
        "Grouping",
        "Profiles",
        "EdgeTraversal",
        "Action",
        "EdgeTraversalOptions",
        "LocalAppPackageId",
        "LocalUserOwner",
        "LocalUserAuthorizedList",
        "RemoteUserAuthorizedList",
        "RemoteMachineAuthorizedList",
        "SecureFlags"
    )
    $json_string = "["
    # Generate header row
    $json_string += "["
    $j = 0
    while ($j -lt $fields.count){
        $field = $fields[$j]
        $json_string += "`"$field`""
        if ($j -lt $fields.count-1){
            $json_string += ","
        }
        $j++
    }
    $json_string += "]"
    # Generate data rows
    $rules = get-firewallrules
    $i = 0
    if ($rules.count -gt 0){
        $json_string += ","
    }
    while ($i -lt $rules.count){
        $rule = $rules[$i]
        $json_string += "["
        $j = 0
        while ($j -lt $fields.count){
            $field = $fields[$j]
            $value = get_field -rule $rule -field $field
            $value = escape-string -value $value
            $json_string += "`"$value`""
            if ($j -lt $fields.count-1){
                $json_string += ","
            }
            $j++
        }
        $json_string += "]"
        if ($i -lt $rules.count-1){
            $json_string += ","
        }
        $i++
    }
    $json_string += "]"
    $data = "{
        `"values`": $json_string,
        `"sheet_name`": `"$sheet_name`",
        `"spreadsheet_id`": `"$spreadsheet_id`"
    }"
    #echo $data > data.txt
    $retval = post -url "https://script.google.com/a/lbl.gov/macros/s/AKfycbwEI4RRKJbRCS8hgMGgqhhlL-3eSAu7m1Fy7-Nzuri3rHSNbttG/exec" -data $data
    #echo $retval > response.html
    return $retval
}

generate_and_log_data -spreadsheet_id $spreadsheet_id -sheet_name $env:COMPUTERNAME
#generate_and_log_data -spreadsheet_id "1WBICetiF-KNQw6-xc8A19v6Lzr9utO7_bHx_LZkemtE" -sheet_name $env:COMPUTERNAME