﻿function write-to-registry {
    Param(
        [string]$value
    )
    $path = "HKLM:\SOFTWARE\LBNL\ICUSW\Windows"
    New-Item -Path $path -Force | Out-Null
    New-ItemProperty -Path $path -Value $value -Name "connected-to-wsus-on-port-80" -Force | Out-Null
}

function write-wsus-connection-status {
    $ipaddress = "wsus.lbl.gov"
    $port = 80
    try {
        $connection = New-Object System.Net.Sockets.TcpClient($ipaddress, $port) -ErrorAction SilentlyContinue
        if ($connection.Connected) {
            write-to-registry -value 1
            Write-Host "Success"
        } else {
            write-to-registry -value 0
            Write-Host "Failed"
        }
    } catch {
        write-to-registry -value 0
        Write-Host "Failed"
    }
}

write-wsus-connection-status