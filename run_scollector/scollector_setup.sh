#!/bin/bash

plist_file="/Library/LaunchDaemons/gov.lbl.icusw.scollector.plist"
systemd_file="/etc/systemd/system/scollector.service"
darwin_scollector_url="https://github.com/bosun-monitor/bosun/releases/download/0.6.0-beta1/scollector-darwin-amd64"
linux_scollector_url="https://github.com/bosun-monitor/bosun/releases/download/0.6.0-beta1/scollector-linux-amd64"
scollector_install_directory="/opt/lbl"

function identify_os {
  is_centos=`cat /etc/*release | grep -i Centos | wc -c`
  is_darwin=`uname | grep -i darwin | wc -c`
}

function create_systemd_service {
echo """
[Unit]
Description=Scollector Server Monitoring

[Service]
Type=simple
Restart=always
User=root
Group=root
RestartSec=10
ExecStart=/opt/lbl/scollector-linux-amd64 -h=nagios-icus.lbl.gov:8070

[Install]
WantedBy=multi-user.target
""" > $systemd_file
}

function create_plist {
echo """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
<key>Label</key>
<string>gov.lbl.icusw.scollector</string>
<key>ProgramArguments</key>
<array>
<string>sh</string>
<string>-c</string>
<string>$scollector_install_directory/scollector-darwin-amd64 -h=nagios-icus.lbl.gov:8070</string>
</array>
<key>RunAtLoad</key>
<true/>
</dict>
</plist>
""" > $plist_file
}

function download_scollector {
mkdir -p $scollector_install_directory
cd $scollector_install_directory
if [ $is_centos -gt 0 ]; then
  curl -L -O $linux_scollector_url
    chmod +x scollector-linux-amd64
  elif [$is_darwin -gt 0 ]; then
    curl -L -O $darwin_scollector_url
    chmod +x scollector-darwin-amd64
  fi
  cd -
}

function reload_plist {
  launchctl unload -w $plist_file
  launchctl load -w $plist_file
}

function stop_scollector {
  if [ $is_centos -gt 0 ]; then
    service scollector stop
    chkconfig scollector off
  elif [ $is_darwin -gt 0 ]; then
    killall scollector-darwin-amd64
    launchctl unload -w $plist_file
  fi
}

function remove_setup_files {
  if [ $is_centos -gt 0 ]; then
    rm $systemd_file
    cd $scollector_install_directory
    rm scollector-linux-amd64
    cd -
  elif [ $is_darwin -gt 0 ]; then
    rm $plist_file
    cd $scollector_install_directory
    rm scollector-darwin-amd64
    cd -
  fi
}

if [ $# -eq 0 ]; then
echo """./scollector_setup.sh
  install
  uninstall"""
else
  if [ `id -u` -ne 0 ]; then
    echo "Error: Setup must be run as root"
    exit 1
  fi
fi

function create_and_start_service {
  if [ $is_centos -gt 0 ]; then
    create_systemd_service
    chkconfig scollector on
    service scollector start
  elif [ $is_darwin -gt 0 ]; then
    create_plist
    reload_plist
  fi
}

identify_os
if [ $(($is_centos + $is_darwin)) -eq 0 ]; then
  echo "Error: OS not implemented"
  exit 2
fi

if [ "$1" == "install" ]; then
  echo "Installing"
  download_scollector
  create_and_start_service
fi

if [ "$1" == "uninstall" ]; then
  echo "Uninstalling"
  stop_scollector
  remove_setup_files
fi
