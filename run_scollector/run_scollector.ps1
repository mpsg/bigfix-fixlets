﻿[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function calc_hash {
    # https://stackoverflow.com/questions/10521061/how-to-get-an-md5-checksum-in-powershell
    Param(
        [string]$Path
    )
    $md5 = New-Object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
    return [System.BitConverter]::ToString($md5.ComputeHash([System.IO.File]::ReadAllBytes($Path)))
}

function log {
    Param(
        [string]$msg
    )
    mkdir "$env:SystemDrive\Options" -Force
    $msg | Add-Content -Path "$env:SystemDrive\Options\fixlet_log.txt"
    echo $msg
}

function generate_config_file {
    mkdir -Path c:\options -Force
    set-content -Path c:\options\scollector.toml -Value "Host = `"http://nagios-icus.lbl.gov:8070`"" -Force
}

function main {
    log -msg "run_scollector.ps1"
    $url = "https://github.com/bosun-monitor/bosun/releases/download/0.6.0-beta1/scollector-windows-amd64.exe"
    $working_directory = $env:SystemDrive + "\Options"
    $target_file = "$working_directory\scollector-windows-amd64.exe"
    mkdir $working_directory -Force
    log -msg "Killing existing scollector processes"
    net stop scollector
    taskkill /IM scollector-windows-amd64.exe /F
    sleep 2
    (New-Object Net.WebClient).DownloadFile($url, $target_file)
    $hash = calc_hash -Path $target_file
    if ($hash -ne "24-45-C3-54-1F-70-C3-69-5E-73-60-6C-89-44-A3-98"){
        log -msg "Error: Bad hash"
        return
    }
    log -msg "Creating config file in c:\options\scollector.toml"
    generate_config_file
    log -msg "Installing scollector service"
    start -FilePath $target_file -ArgumentList "-winsvc=`"install`"" -WindowStyle Hidden -Wait
    log -msg "Starting scollector service"
    net start scollector
}

main
