#!/bin/bash
url="https://bitbucket.org/mpsg/crowdstrikeinstall/raw/master/CSInstall.dmg"
dmg="CSInstall.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
/Library/Application\ Support/Sophos/opm/Installer.app/Contents/MacOS/tools/InstallationDeployer --force_remove 2>/dev/null
# Install new
installer -verboseR -package ./$dir/*.pkg -target /
# register
/Applications/Falcon.app/Contents/Resources/falconctl license 983704422DD24004A97F084369CBBFE4-5E
/Applications/Falcon.app/Contents/Resources/falconctl grouping-tags set "BLGeneralMac"
/Applications/Falcon.app/Contents/Resources/falconctl unload
/Applications/Falcon.app/Contents/Resources/falconctl load
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir