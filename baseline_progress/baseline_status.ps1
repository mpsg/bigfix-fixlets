﻿Param(
    [string]$datasource_url = "https://icusw.lbl.gov"
)

# This is a shim for where JSON parsing is not available. Previously used system.web.script for JSON deserialization but later found that it's only available in .NET 3.5
$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json_shim.ps1"
Import-Module -Name $json_file -Force
$in_domain = (Get-WmiObject -Class Win32_ComputerSystem).PartOfDomain
$bigfix_installed = (Test-Path "C:\Program Files (x86)\BigFix Enterprise\BES Client\qna.exe")
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function ConvertFrom-JsonShim([object] $item){ 
    #add-type -assembly system.web.extensions
    #$ps_js=new-object system.web.script.serialization.javascriptSerializer

    #The comma operator is the array construction operator in PowerShell
    #return $ps_js.DeserializeObject($item)
    return [Procurios.Public.JSON]::JsonDecode($item)
}

function psobject_to_hash {
    Param (
        [PSObject]$object
    )
    $retval = @{}
    $object.psobject.properties | Foreach { $retval[$_.Name] = $_.Value }
    return $retval
}

function dump-json-at {
    Param(
        [string]$Path,
        [PSCustomObject]$Object,
        [int]$count
    )
    New-Item -Path $path -Force | Out-Null
    if ($Object -eq $null){
        return
    }
    if ($Object.getType().ToString() -like "*PSCustomObject*"){
        $Object = psobject_to_hash($Object)
    }
    foreach ($key in $Object.keys){
        $value = $Object[$key]
        if ($value -ne $null){
            if (
                $value.gettype().toString() -like "*Dictionary*" -or
                $value.gettype().name -like "*Hashtable*" -or
                $value.gettype().toString() -like "*PSCustomObject*"
            ){
                $count += dump-json-at -Path "$path\$key" -Object $value
            } else {
                New-ItemProperty -Path $path -Value $value -Name $key -Force | Out-Null
                $count += 1
            }
        }
    }
    return $count
}

function write_property_data_from {
    Param(
        [String]$from
    )
    $data = property_data_from($from)
    if ($data -ne $null){
        log -msg "Writing data into the registry"
        $path = "HKLM:\SOFTWARE\LBNL\ICUSW"
        $records_written = dump-json-at -Path $path -Object $data -count 0
        log -msg "Wrote $records_written records"
    } else {
        log -msg "write_property_data_from tried to process null `$data"
    }
}

function log {
    Param(
        [string]$msg
    )
    $log_folder = "$env:SystemDrive\Options\tag_property_data"
    $result = mkdir $log_folder -Force
	$timestamp = Get-Date -UFormat "%Y%m%d %H:%M:%S"
	$msg = $timestamp + ": $msg"
    $msg | Add-Content -Path "$log_folder\log.txt"
    Write-Host $msg
}

function baseline_data_from {
    Param(
        [String]$server
    )
    $url = $server + "/api/get_baseline_relevances?name=ICUSW+Win"
    $WebClient = New-Object System.Net.WebClient
    try {
        $raw_json = $WebClient.DownloadString($url)
        #log -msg "Retrieved property data: $raw_json"
    } catch {
         #while ($true){ $input = Read-Host -Prompt "repl"; if ($input -eq "exit"){ return 0 }; Invoke-Expression $input }
         log -msg $Error
         return $null
    }
    return ConvertFrom-JsonShim $raw_json
}

function get_serial_number {
	$retval = (Get-WmiObject -Class Win32_BIOS).SerialNumber
	log -msg "Retrieved serial number: $retval"
    return $retval
}

function evaluate_relevance {
    Param(
        [String]$relevance
    )
    $tmp_file = "C:\tmp\component_relevance.rel"
    mkdir -Path "C:\tmp" -Force -ErrorAction SilentlyContinue | Out-Null
    echo "Q: $relevance" > $tmp_file
    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.FileName = "C:\Program Files (x86)\BigFix Enterprise\BES Client\qna.exe"
    $pinfo.RedirectStandardError = $true
    $pinfo.RedirectStandardOutput = $true
    $pinfo.UseShellExecute = $false
    $pinfo.Arguments = $tmp_file
    $pinfo.WindowStyle = "Hidden"
    $p = New-Object System.Diagnostics.Process
    $p.StartInfo = $pinfo
    $p.Start() | Out-Null
    $p.WaitForExit()
    $stdout = $p.StandardOutput.ReadToEnd()
    $stderr = $p.StandardError.ReadToEnd()
    #Write-Host "stdout: $stdout"
    #Write-Host "stderr: $stderr"
    #Write-Host "exit code: " + $p.ExitCode
    $relevance_result = $stdout.Split("A:")
    $relevance_result = $relevance_result[$relevance_result.Count-1].Trim()
    return $relevance_result
}

function DellCommandUpdate {
    $tmp_file = "C:\tmp\dcu.xml"
    mkdir -Path "C:\tmp" -Force -ErrorAction SilentlyContinue | Out-Null
    Start-Process -WorkingDirectory "c:\Program Files (x86)\Dell\CommandUpdate\" -FilePath "dcu-cli.exe" -ArgumentList "/report $tmp_file" -Wait -WindowStyle Hidden
    #$result = [xml](Get-Content -Path "dcu.xml")
}

function print_baseline_progress {
    Param(
        [PSObject]$baseline
    )
    ForEach($component_name in $baseline.Keys){
        $relevances = $baseline[$component_name]
        $component_done = ""
        $color = "White"
        ForEach($relevance in $relevances){
            $relevance = $relevance.Replace("`n", "")
            $relevance_result = evaluate_relevance -relevance $relevance
            if ($relevance_result -eq "True"){
                $component_done = "Relevant"
                $color = "Red"
                break
            } elseif ($relevance_result -eq "False"){
                $component_done = "Not Relevant"
            } else {
                $component_done = "Unsure"
                $color = "Yellow"
                break
            }
        }
        if ($component_done -eq "Not Relevant"){
            $color = "Green"
        }
        write-host "`t$component_done`t" -ForegroundColor $color -NoNewline
        write-host "$component_name"
    }
    Write-Host
}

function get_ad_description
{
    $searcher = New-Object System.DirectoryServices.DirectorySearcher([ADSI]'')
    $searcher.Filter = "(&(objectClass=Computer)(cn=$($env:COMPUTERNAME)))"
    $description = $searcher.FindOne().GetDirectoryEntry().description
    return $description
}

function print_secondary_checks
{
    $default_console_color = [console]::ForegroundColor
    $windows_activation_relevance = "not (((select `"LicenseStatus from SoftwareLicensingProduct WHERE (PartialProductKey is not null) and (Name like 'Windows(R)%25')`" of wmi) as string contains `"1`"))"
    $pending_dcu_relevance = 'not ((number of child nodes of selects "/updates" of xml document of file "C:\tmp\dcu.xml") is 0)'
    $property_tagged = 'not ((exists (key "HKEY_LOCAL_MACHINE\SOFTWARE\LBNL\ICUSW\dw_property" of registry)) or (exists (key "HKEY_LOCAL_MACHINE\SOFTWARE\LBNL\ICUSW\dw_property" of native registry)))'
    $checks = @{
        "Windows Activation" = $windows_activation_relevance
        #"Pending Dell Command Updates" = $pending_dcu_relevance
        "Property Autotagged" = $property_tagged
    }
    ForEach($check_name in $checks.Keys){
        $relevance = $checks[$check_name]
        $relevance_result = evaluate_relevance -relevance $relevance
        if ($relevance_result -eq "True"){
            $component_done = "Relevant"
            $color = "Red"
        } elseif ($relevance_result -eq "False"){
            $component_done = "Not Relevant"
            $color = "Green"
        } else {
            $component_done = "Unsure    "
            $color = "Yellow"
        }
        write-host "`t$component_done`t" -NoNewline -ForegroundColor $color
        write-host "$check_name"
    }
    Write-Host
}

function print_missing_gpos {
    $default_console_color = [console]::ForegroundColor
    $history = Get-GPOHistory
    $applicable = Get-ComputerAppliedGPOs
    write-host "`t$($applicable.cn)" -ForegroundColor Yellow
    ForEach($gpo in $applicable.gpos){
        if ($gpo -in $history){
            $color = "Green"
        } else {
            $color = "Red"
        }
        write-host "`t$gpo" -ForegroundColor $color
    }
    Write-Host
}

function print_fixtlets {
    Write-Host "`tLast 10 Fixed Fixlets" -ForegroundColor Yellow
    Get-Content "C:\Program Files (x86)\BigFix Enterprise\BES Client\__BESData\__Global\Logs\$(Get-Date -Format "yyyyMMdd").log" | sls "Fixed"| Select-Object -Last 10 | % {
        $line = $_.ToString().Trim()
        write-host "`t$line" -ForegroundColor Green
    }
    Write-Host
}

function print_header {
    if ($in_domain){
        $description = get_ad_description
        Write-Host "AD Description:`t$description"
    } else {
        Write-Host "Computer is not in a domain" -ForegroundColor Yellow
    }
    if (-not (Test-Path "c:\Program Files (x86)\Dell\CommandUpdate\dcu-cli.exe")){
        Write-Host "Dell Command Update not found: c:\Program Files (x86)\Dell\CommandUpdate\dcu-cli.exe" -ForegroundColor Yellow
    }
    Write-Host
}

function Get-ComputerAppliedGPOs {
    $gpos = @()
    $output = gpresult /r
    $found = $false
    $cn = ""
    ForEach($line in $output){
        $line = $line.Trim()
        if ($line -match "CN=.*"){
            $cn = $line
        }
        if ($line.Contains("Applied Group Policy Objects")){
            $found = $true
            continue
        }
        if ($found){
            if ($line -eq ""){
                break
            }
            if ($line -match "^-+$"){
                continue
            }
            $gpos += $line
        }
    }
    return @{
        gpos = $gpos
        cn = $cn
    }
}

function Get-GPOHistory {
    $history = get-childitem "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy\History\"
    $history | Get-ChildItem | % { $_.GetValue("DisplayName") } | Sort-Object | Get-Unique
}

if (! $host.name.Contains("Windows PowerShell ISE Host")){
    if (-not $bigfix_installed){
        $default_console_color = [console]::ForegroundColor
        [console]::ForegroundColor = "Red"
        Write-Host "BigFix not detected: C:\Program Files (x86)\BigFix Enterprise\BES Client\qna.exe"
        [console]::ForegroundColor = $default_console_color
        pause
        exit 1
    }
    if ($datasource_url -eq $null){
        $datasource_url = "https://icusw.lbl.gov"
    }
    $baseline = baseline_data_from($datasource_url)
    $counter = 0
    while ($true){
        cls
        print_header
        Write-Host "Baseline Checks"
        print_baseline_progress -baseline $baseline
        Write-Host "Secondary Checks"
        print_secondary_checks
        Write-Host "Missing GPOs"
        if ($in_domain){
            print_missing_gpos
        } else {
            Write-Host "`tNot in domain`n"
        }
        Write-Host "Fixlets"
        if ($bigfix_installed){
            print_fixtlets
        } else {
            Write-Host "`tBigFix not installed`n"
        }
        if ($counter % 5 -eq 0 -and $bigfix_installed){
            Restart-Service -Name "BESClient"
        }
        Sleep -Seconds 60
        $counter += 1
    }
}
